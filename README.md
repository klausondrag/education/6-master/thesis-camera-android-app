# Camera APP

This repo contains an android app which communicates with the corresponding REST Api. 
When run, it connects to the API, takes pictures on the press of a button and shows the taken pictures.

## Setup
To generate an apk, please use android studio.

## Running the app
1. Connect the computer with the camera over USB
1. Test the camera setup by running the demo in the API project
1. Run the REST API on your computer
1. The computer and the phone need to be on the same network.
It is not necessary that the computer and phone have internet access.
The connection happens over the network without internet because outside mobile connection might not be possible.
So you can disable mobile data to make sure it does not get used up.
    - For testing, this can be the wifi of a router.
    - For running this in the field or outside, you can create a wifi hotspot with your phone.
1. Start the app on your phone
1. Enter the URL of your computer, for example `http://192.168.1.2:8080`
1. Test the connection
1. Take some pictures
